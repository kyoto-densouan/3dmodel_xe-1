# README #

1/3スケールの電波新聞社製ジョイスティック XE-1風小物のstlファイルです。 
一部省略しています。 組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 
元ファイルはAUTODESK 123D DESIGNです。 

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_xe-1/raw/656fdb04ab92fbd584bd5540db0678bad020b7d3/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_xe-1/raw/656fdb04ab92fbd584bd5540db0678bad020b7d3/ExampleImage.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_xe-1/raw/656fdb04ab92fbd584bd5540db0678bad020b7d3/ExampleImage_2.jpg)
![](https://bitbucket.org/kyoto-densouan/3dmodel_xe-1/raw/656fdb04ab92fbd584bd5540db0678bad020b7d3/ExampleImage_3.jpg)
